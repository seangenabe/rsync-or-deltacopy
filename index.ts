import { spawn } from "child_process"

export async function getPath(): Promise<string | null> {
  const pathRsync = await new Promise((r, j) => {
    const proc = spawn("rsync", ["-h"])
    proc.on("exit", x => {
      if (typeof x === "string" || x !== 0) {
        j(new Error(`rsync exited with code ${x}`))
        return
      }
      r(true)
    })
    proc.on("error", () => r(false))
  })
  if (pathRsync) {
    return "rsync"
  }
  try {
    const { rsync } = await import("deltacopy")
    return rsync
  } catch (err) {
    return null
  }
}
