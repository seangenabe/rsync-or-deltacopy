# rsync-or-deltacopy

Gets the path of rsync in Unix or DeltaCopy/rsync in Windows

## Usage

```typescript
import { getPath } from "rsync-or-deltacopy"

await getPath()
```
